## Nom du projet
Equalizer Audio

## Membres du projet
* Jessy Cauquy
* Bruno Folcher

## Desription du projet
Création d'une Publicité sur Lieu de Vente (PLV) pour la marque Bose.
Bose a mis en vente sa dernière enceinte pour téléphones, et veux mettre en avant sa qualité.
Pour la démontrer, nous allons créer un equalizer qui analysera les sons en sortie des téléphones et de l'enceinte.
L'equalizer affichera la décomposition du son en barres de différentes fréquences sur une LED matrix.
Cet affichage graphique est directement compris par les clients qui viendront tester le son de leur téléphone, et de l'enceinte de test.
Plus les barres sont nombreuses et hétérogènes, et plus l'appareil qui émet le son est de bonne qualité (pour des musiques le permettant).

Matériel utilisé :
* Carte Arduino Mega
* LED Matrix 16x32
* Potentiomètre (pour augmenter/diminuer la sensibilité du récepteur)
* Résistances
* Capteurs de son
* Breadboard
* Fils de connexion


## Organisation du projet (29/11/2017)
Étapes :
* Définition et Brainstorming sur le projet --> FINI le 13/11
* Rédaction complète du cahier des charges (GdP) --> EN COURS le 29/11/2017 (étape planification)
