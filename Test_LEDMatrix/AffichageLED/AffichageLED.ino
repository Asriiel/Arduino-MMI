#include <Adafruit_GFX.h>
#include <RGBmatrixPanel.h>
#include <SPI.h>

#define CLK 11
#define LAT A3
#define OE  9
#define A   A0
#define B   A1
#define C   A2
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, false);


void setup() {
  matrix.begin();
  matrix.drawPixel(1, 1, matrix.Color333(4, 4, 4));
}
void loop() {
  // do nothing
}

